#ifndef JOGO_H
#define JOGO_H

#include <stdlib.h>
#include <string>
#include <fstream>
#include <iostream>
#include <ctime>
#include <math.h>
#include "Singleton.h"

using namespace std;

/**
 * @authors Gustavo Lofrese Carvalho
 * @date 11/12/2021
 * @version 06/02/2022
 * @brief An Class to store Game stuffs
**/
class Jogo : public Singleton<Jogo> {
private:
    int pontuacao;

    Jogo(int tamanhoTela);

    Jogo();

    ~Jogo();

    // Jogo consegue acessar os métodos da classe Singleton
    friend class Singleton<Jogo>;

public:
    int getPontuacao();

    void dificuldade();

    string getLastLine(fstream &in);

    void imprimePontuacao();

    int sortearNumero(int min, int max);

    double velocidade;
    int intervalo;

    // Método para obter a instância do Singleton
    static Jogo &getInstance(int tamanhoTela) {
        return Singleton<Jogo>::getInstance(tamanhoTela);
    }
};

#endif // JOGO_H
