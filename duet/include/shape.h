#ifndef J1_SHAPE_H
#define J1_SHAPE_H

#include "window.h"
#include "rotatable.h"
#include "drawable.h"

/**
 * @author Éder Augusto Penharbel, Gustavo Lofrese Carvalho
 * @date 11/11/2021
 * @version 11/01/2022
 * @brief An abstract Class to store a Geometrical Shape
 **/
class Shape : public Rotatable, public Drawable
{
public:
	Rotatable *rotateBehavior;
	Drawable *drawBehavior;

	void draw(Window &w);
	void rotacao(double da);
};

#endif
