
#ifndef POLYGON_FACTORY_H
#define POLYGON_FACTORY_H

#include "shape.h"
#include "point2.h"
#include "window.h"
#include "matriz.h"
#include "polygon.h"

class PolygonFactory:public Shape
{

public:
    static Polygon* Create(int nPontos, Jogo& pJogo, double pTamanho);
};

#endif