#ifndef J1_WINDOW_H
#define J1_WINDOW_H

#include <SDL.h>

#include "point2.h"

/**
* @author Gustavo Lofrese Carvalho, Eder
* @date 19/10/2021
* @version 13/12/2021
**/

class Window 
{
    protected:
	    //The window we'll be rendering to
    	SDL_Window * window;
        //The window renderer
        SDL_Renderer * renderer = NULL;
    public:
        Window(int width, int height);
        ~Window();

        int width;
        int height;
        void drawLine(int xi, int yi, int xf, int yf);
        void drawLine(Point2 pi, Point2 pf);
        void drawCircle(int cX,int cY,int r); //função que irá desenhar o círculo em si
        void clear();
        void update();
};

#endif
