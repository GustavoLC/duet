#ifndef __DRAWBYPOINTS_H__
#define __DRAWBYPOINTS_H__

#include "drawable.h"

class DrawPolygon : public Drawable
{
private:
    int numberOfPoints;
    Point2 *points;

public:
    DrawPolygon(int numberOfPoints, Point2 *point);
    void draw(Window &w);
};

#endif