#ifndef Matriz_H
#define Matriz_H

/**
* \author Gustavo Lofrese Carvalho, Éder Augusto Penharbel
* \date 11/11/2021
* @version 17/12/2021
* \brief An abstract Class to work with matrix
**/

class Matriz
{
private:
    int linhas;
    int colunas;

public:
    Matriz(int linhas, int colunas);
    Matriz(const Matriz& m);
    Matriz(Matriz &&m);
    ~Matriz();
    
    double **a;
    void inserirValores(int linhas, int colunas, double valor);  //inicializar uma Matriz com valores inseridos
    void imprimir();    //imprimir a Matriz 
    Matriz &operator =(const Matriz& m); //operador igual, deleta a Matriz original e retorna a nova
    Matriz &operator +=(int v); //operador para somar uma Matriz com número inteiro(pode utilizar (a + (-2)), por exemplo)
    Matriz &operator +=(const Matriz& m);   //operador para somar Matriz com Matriz
    Matriz &operator *=(int v); //operador para multiplicar Matriz com número inteiro
    Matriz &operator *=(const Matriz& m); //operador para multiplicar Matriz com Matriz 
    friend Matriz operator +(const Matriz& m, int v);   //função amiga, soma de Matriz com inteiro
    friend Matriz operator +(int v, const Matriz& m);   //função amiga, soma de inteiro com Matriz
    friend Matriz operator +(const Matriz& m, const Matriz& n); //função amiga, soma de Matriz com Matriz
    friend Matriz operator *(const Matriz& m, int v); //função amiga, multiplicação de Matriz por inteiro
    friend Matriz operator *(int v,const Matriz& m);    //função amiga, multiplicação de inteiro por Matriz
    friend Matriz operator *(const Matriz& m, const Matriz& n); //função amiga, multiplicação de Matriz por Matriz
};

#endif
