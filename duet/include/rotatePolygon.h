#ifndef __ROTATEPOLYGON_H__
#define __ROTATEPOLYGON_H__

#include "rotatable.h"
#include "point2.h"

class RotatePolygon : public Rotatable
{
private:
    int numberOfPoints;
    Point2 *points;

    double minimox;
    double minimoy;
    double maximox;
    double maximoy;

public:
    RotatePolygon(int numberOfPoints, Point2 *points);
    void rotacao(double da);

    double getMinX() const;
    double getMinY() const;
    double getMaxX() const;
    double getMaxY() const;
};

#endif