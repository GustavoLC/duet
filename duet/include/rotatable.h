#ifndef ROTATABLE_H
#define ROTATABLE_H

class Rotatable
{
public:
    virtual void rotacao(double da) = 0;
    virtual ~Rotatable() = 0;
};

#endif