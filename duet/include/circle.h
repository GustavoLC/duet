#ifndef CIRCLES_H
#define CIRCLES_H

#include "window.h"
#include "point2.h"
#include "shape.h"
#include "polygon.h"
#include "retangulo.h"

/**
 * @authors Gustavo Lofrese Carvalho, Éder Augusto Penharbel
 * @date 10/11/2021
 * @version 11/01/2022
 * @brief An abstract Class to store a Circle, heritage from Shape class
 */
class Circle : public Shape
{
public:
    Circle(Point2 *c, double r);
    ~Circle();

    Point2 *c;
    double r;

    bool colisaoCQ(Polygon *p);
};

#endif