#ifndef __DRAWCIRCLE_H__
#define __DRAWCIRCLE_H__

#include "drawable.h"

class DrawCircle : public Drawable
{
private:
    Point2 *centro;
    int raio;

public:
    void draw(Window &w);
    DrawCircle(Point2 *centro, int r);
};

#endif