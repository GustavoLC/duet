#ifndef __DRAWABLE_H__
#define __DRAWABLE_H__

#include "window.h"

class Drawable
{
public:
    virtual void draw(Window &w) = 0;
    virtual ~Drawable() = 0;
};

#endif