#ifndef __NOROTATE_H__
#define __NOROTATE_H__

#include "rotatable.h"

class NoRotate : public Rotatable
{
public:
    void rotacao(double da);
};

#endif