#include "shape.h"

void Shape::rotacao(double da)
{
    rotateBehavior->rotacao(da);
}

void Shape::draw(Window &w)
{
    drawBehavior->draw(w);
}
