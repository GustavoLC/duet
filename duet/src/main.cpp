#include <iostream>
#include <vector>
#include <cmath>
#include <cstdio>
#include <fstream>
#include <string>

using namespace std;

#include "window.h"
#include "sdl.h"
#include "shape.h"
#include "rotatable.h"
#include "drawable.h"
#include "polygon.h"
#include "quadrilateral.h"
#include "circle.h"
#include "duet.h"
#include "retangulo.h"
#include "jogo.h"
#include "matriz.h"
#include "Singleton.h"
#include "Singleton.cpp"
#include "PolygonFactory.h"
#include "PolygonFactory.cpp"

int main()
{
    // Utiliza o getInstance para instanciar o sdl
    SDL &sdl = SDL::getInstance();
    sdl.init();
    bool quit = false;

    Window w(800, 800);
    Jogo &jogo = Jogo::getInstance(w.width + w.height);

    Duet duet(
        Point2(w.width / 2, w.height * 75 / 100), // ponto central
        120,                                      // raio
        Circle(new Point2(400, 600), 30),         // c1
        Circle(new Point2(400, 600), 30),         // c2
        0                                         // alpha
    );
    duet.rotacao(0);

    /**
     * @brief Vector que irá acumular os polygons
     * @authors Gustavo Lofrese Carvalho, Eduardo Meneghim, Eder Penharbel
     * @date 01/12/2021
     */
    vector<Polygon *> polygons;
    int t = 0;

    // Event handler
    SDL_Event e;
    {
        srand(time(NULL));

        // Loop de Jogo
        while (!quit)
        {
            /**
             *@author Gustavo Lofrese Carvalho, Eduardo Meneghim, Eder Augusto Penharbel
             *@date 29/11/2021
             *@version 13/12/2021
             * @brief gera polygons automaticamente
             * @param t tempo de execucao
             **/

            if (t % jogo.intervalo == 0)
            {
                int tamanho = jogo.sortearNumero(50, 70); // Sorteia a altura do retangulo

                Polygon *r1 = PolygonFactory::Create(4, jogo, tamanho);

                // Faz um retângulo girar, outro não
                if (polygons.size() > 0)
                    r1->deveGirar = !polygons.back()->deveGirar;

                polygons.push_back(r1);
            }
            t++;

            // Call the Score function
            jogo.dificuldade();

            // Handle events on queue
            while (SDL_PollEvent(&e))
            {
                // comandos de teclado, fazer os dois circulos se movimentarem
                if (e.type == SDL_KEYDOWN)
                {
                    switch (e.key.keysym.sym)
                    {
                    case SDLK_LEFT:
                        duet.rotacao(-(M_PI / 30));
                        break;

                    case SDLK_RIGHT:
                        duet.rotacao(M_PI / 30);
                        break;

                    default:
                        break;
                    }
                }

                // User requests quit
                if (e.type == SDL_QUIT)
                {
                    jogo.imprimePontuacao();
                    quit = true;
                }
            }

            // clear surface
            w.clear();

            /**
             *@author Gustavo Lofrese Carvalho, Eduardo Meneghim
             *@date 01/12/2021
             *@version 31/01/2022
             *@brief Gera, desenha, movimenta, faz a colisão e remove os polygons
             **/
            for (size_t i = 0; i < polygons.size(); i++)
            {

                polygons[i]->draw(w);                        // desenha os polygons
                polygons[i]->transladar(0, jogo.velocidade); // desce polygons automaticamente de acordo com a velocidade

                if ((duet.c1.colisaoCQ(polygons[i]) || duet.c2.colisaoCQ(polygons[i])) &&
                    (polygons[i]->colisaoR(duet.c1.c, duet.c1.r) || polygons[i]->colisaoR(duet.c2.c, duet.c2.r)))
                {
                    jogo.imprimePontuacao();
                    sdl.quit();
                    return 0;
                }

                // Apaga os retângulos assim que eles saem da tela
                if (polygons[i]->getMinY() > w.height + 200)
                    polygons.erase(polygons.begin());

                // Se o retângulo possuir deveGirar = true, ele gira
                if (polygons[i]->deveGirar == true)
                    polygons[i]->rotacao(M_PI / 3000); // angulo
            }

            // draw duet
            duet.draw(w);

            // update renderer
            w.update();
        }
        // Fim do loop de jogo
    }
    sdl.quit();
    return 0;
}