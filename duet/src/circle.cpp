#include <stdlib.h>
#include <iostream>

#include "circle.h"
#include "noRotate.h"
#include "drawCircle.h"
#include <math.h>

using namespace std;

/**
 * Constructor
 * @authors Gustavo Lofrese Carvalho, Éder Augusto Penharbel
 * @date 10/11/2021
 * @version 10/11/2021
 * @param c, the center of the circle
 * @param r, the radius of the circle
 * @brief Circle constructor with Point2
 */
Circle::Circle(Point2 *c, double r) : r{r}
{
  Circle::c = c;
  rotateBehavior = new NoRotate();
  drawBehavior = new DrawCircle(Circle::c, r);
}

/**
 * @authors Gustavo Lofrese Carvalho, Éder Augusto Penharbel
 * @brief Destroy the Circle:: Circle object
 *@date 10/11/2021
 *@version 10/11/2021
 */
Circle::~Circle()
{
}

/**
 * @author Gustavo Lofrese Carvalho, Eduardo Meneghim
 * @date 29/11/2021
 * @version 31/01/2022
 * @brief test for collision between rectangles and circles
 * @param p References a Rectangle
 **/
bool Circle::colisaoCQ(Polygon *p)
{

  // variáveis temporárias para usarmos nos cálculos
  float valorX = c->x;
  float valorY = c->y;

  // Verifica qual canto é o mais próximo
  if (c->x < p->getMinX())
    valorX = p->getMinX(); // testa esquerda

  else if (c->x > p->getMaxX())
    valorX = p->getMaxX(); // testa direita

  if (c->y < p->getMinY())
    valorY = p->getMinY(); // // testa cima

  else if (c->y > p->getMaxY())
    valorY = p->getMaxY(); // testa baixo

  // Pega a distância do canto mais próximo
  float distX = c->x - valorX;
  float distY = c->y - valorY;
  float distancia = sqrt((distX * distX) + (distY * distY));

  // Caso a distância seja menor que o raio, significa que há colisão
  if (distancia <= r)
  {
    return true;
  }
  return false;
}
