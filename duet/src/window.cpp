/**
*\author Gustavo Lofrese Carvalho, Eder
*\date 10/11/2021
Ultima atualização: Função drawCircle
**/

#include "window.h"
#include <iostream>
using namespace std;

Window::Window(int width, int height):
    width{width},
    height{height}
{
    window = SDL_CreateWindow( "SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, 
        SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_SHOWN );
    if (window == NULL)
    {
        cerr << "Window could not be created! SDL_Error: " << SDL_GetError() << endl;
        exit(1);
    }
	//Create renderer for window
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
}

Window::~Window()
{
	//Destroy window
	SDL_DestroyWindow(window);
    cout << "encerrando o objeto" << endl;
}

void Window::drawLine(int xi, int yi, int xf, int yf)
{
    //Draw blue horizontal line
    SDL_SetRenderDrawColor(renderer, 255, 17, 0xFF, 0xFF);		
    SDL_RenderDrawLine(renderer, xi, yi, xf, yf);
}
    
void Window::drawLine(Point2 pi, Point2 pf)
{
    drawLine(pi.x, pi.y, pf.x, pf.y);
}

/**
* \author Gustavo Lofrese de Carvalho
* \date 10/11/2021
* \brief função que irá desenhar o círculo em si - "Midpoint Circle Drawing Algorithm"
**/

void Window::drawCircle(int cX,int cY,int r)
{
    int x = 0; 
    int y = r;
    int tx = 0;
    int ty = 0;
    int error = (tx - (r*2)); //TODO: achar explicação (@oederaugusto para @GustavoLC: acho que
    // tem a explicação no Livro Computer Graphics, C version - Donald Hearn e Pauline M. Baker.

    while (x <= y)
   {
        //imprime os octantes
        SDL_RenderDrawPoint(renderer, cX + x, cY - y);  // octante 1
        SDL_RenderDrawPoint(renderer, cX + x, cY + y);  // octante 2
        SDL_RenderDrawPoint(renderer, cX - x, cY - y);
        SDL_RenderDrawPoint(renderer, cX - x, cY + y);      //.
        SDL_RenderDrawPoint(renderer, cX + y, cY - x);      //.
        SDL_RenderDrawPoint(renderer, cX + y, cY + x);      //.
        SDL_RenderDrawPoint(renderer, cX - y, cY - x);
        SDL_RenderDrawPoint(renderer, cX - y, cY + x); // octante 8 
        
        // gera os novos pontos, incrementando 'x' e decrementando 'y' para desenhar a circunferencia
        if (error <= 0)
        {
            ++x;
            error += tx;
            tx += 2;
        }
        
        else if (error > 0)
        {
            --y;
            ty += 2;
            error += (ty - (r*2));
        }
   }
}

void Window::clear()
{
    //Clear screen
    SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xFF);
    SDL_RenderClear(renderer);
}

void Window::update()
{
	//Update screen
    SDL_RenderPresent(renderer);
}
