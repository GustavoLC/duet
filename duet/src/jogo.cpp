#include "jogo.h"


using namespace std;

/**
 * @authors Gustavo Lofrese Carvalho
 * @date 11/12/2021
 * @version 11/12/2021
 * @brief Game Constructor
**/
Jogo::Jogo()
{}

/**
 * @authors Gustavo Lofrese Carvalho, Eduardo Meneghim
 * @date 13/12/2021
 * @version 13/12/2021
 * @brief Game Constructor to get Window size
 * @param tamanhoTela get the sum of the height and width of the screen
**/
Jogo::Jogo(int tamanhoTela)
{
    if (tamanhoTela >= 2400)
    {
        velocidade = 0.8;
        intervalo = 600;
    }
    else if (tamanhoTela >= 1600) 
    {
        velocidade = 0.3;
        intervalo = 1500;
        }
    else if(tamanhoTela >= 1200) 
    {
        velocidade = 0.15;
        intervalo = 2000;       
    }
    else           
    {
        velocidade = 0.1;
        intervalo = 5000;
    }
}

/**
 * @authors Gustavo Lofrese Carvalho
 * @date 11/12/2021
 * @version 11/12/2021
 * @brief Game Destructor
**/
Jogo::~Jogo()
{}

/**
 * @brief Makes the score of the game
 * @authors Gustavo Lofrese Carvalho
 * @date 11/12/2021
 * @version 11/12/2021
 * 
 */
int Jogo::getPontuacao()
{
    pontuacao = clock() / 50000;
    return pontuacao;
}

/**
 * @authors Gustavo Lofrese Carvalho
 * @date 13/12/2021
 * @version 13/12/2021
 * @brief Changes the game dificulty over time
 */
void Jogo::dificuldade()
{
    pontuacao = clock() / 50000;
    
    if (pontuacao % 200 == 0 )
        velocidade += 0.001;
}

/**
 * @brief Gera um valor inteiro entre um minimo e maximo informado
 * @date 09/02/2022
 * @version 09/02/2022
 * @author Gustavo Lofrese Carvalho
 * @param min valor minimo
 * @param max valor maximo
 * @return int 
 */
int Jogo::sortearNumero(int min, int max)
{
    //Código para gerar um número aleatório entre um mínimo e um máximo
    return min + fmod(rand(),(max - min + 1));
}

/**
 * @author Gustavo Lofrese Carvalho
 * @brief Percorre um arquivo e retorna a última linha
 * @date 06/02/2022
 * @version 06/02/2022
 * @param in arquivo de leitura
 * @return string - a ultima linha
 */
string Jogo::getLastLine(fstream& in)
{
    string line;
    while (in >> ws && getline(in, line)) // skip empty lines
        ;
    return line;
}

/**
 * @authors Gustavo Lofrese Carvalho, Eduardo Meneghim
 * @brief Imprime as duas últimas pontuações
 * @date 06/02/2022
 * @version 07/02/2022
 */
void Jogo::imprimePontuacao() 
{
    fstream arquivo; // Abre o arquivo

    string a = to_string(getPontuacao()); // Coloca o valor da pontuação na variável a, convertendo para string
    cout << "Você fez " << a << " pontos nessa rodada" << endl;

    string b = "0"; // Inicia o valor da string B com 0
    

    arquivo.open("Record.txt", ios::in); //Abre um arquivo para leitura

    if (arquivo.is_open())
    {
        b = getLastLine(arquivo); // Pega a maior pontuação e atribui para b
        if (stoi(a) > stoi(b)) //Verifica se é a maior pontuação
            b = a;
        
        cout << "Sua maior pontuação foi de: " << b << endl;
    }
    arquivo.close(); //Fecha o arquivo


    arquivo.open("Record.txt",ios::out | ios::trunc); //Abre um arquivo para escrita e apaga as pontuações antigas
    arquivo << b << endl; //Insere a pontuação no arquivo
    arquivo.close(); //Fecha o arquivo
}