#include "drawPolygon.h"

DrawPolygon::DrawPolygon(int numberOfPoints, Point2 *points) : numberOfPoints{numberOfPoints}, points{points}
{
}

void DrawPolygon::draw(Window &w)
{
    for (int i = 0; i < numberOfPoints - 1; i++)
    {
        w.drawLine(points[i], points[i + 1]);
    }
    w.drawLine(points[numberOfPoints - 1], points[0]);
}
