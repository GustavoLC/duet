#include "rotatePolygon.h"
#include "matriz.h"
#include <cmath>

RotatePolygon::RotatePolygon(int numberOfPoints, Point2 *points) : numberOfPoints{numberOfPoints}, points{points}
{
}

void RotatePolygon::rotacao(double da)
{
    double dx = (getMaxX() + getMinX()) / 2.0; // centro X
    double dy = (getMaxY() + getMinY()) / 2.0; // centro Y

    // matrix to make the rectangle rotate
    Matriz rotacao(3, 3);
    rotacao.inserirValores(1, 1, cos(da));
    rotacao.inserirValores(1, 2, -(sin(da)));
    rotacao.inserirValores(1, 3, 0);

    rotacao.inserirValores(2, 1, sin(da));
    rotacao.inserirValores(2, 2, cos(da));
    rotacao.inserirValores(2, 3, 0);

    rotacao.inserirValores(3, 1, 0);
    rotacao.inserirValores(3, 2, 0);
    rotacao.inserirValores(3, 3, 1);

    // matrix to make the rectangle go to the origin
    Matriz translacaoOrigem(3, 3);
    translacaoOrigem.inserirValores(1, 1, 1);
    translacaoOrigem.inserirValores(1, 2, 0);
    translacaoOrigem.inserirValores(1, 3, -dx);

    translacaoOrigem.inserirValores(2, 1, 0);
    translacaoOrigem.inserirValores(2, 2, 1);
    translacaoOrigem.inserirValores(2, 3, -dy);

    translacaoOrigem.inserirValores(3, 1, 0);
    translacaoOrigem.inserirValores(3, 2, 0);
    translacaoOrigem.inserirValores(3, 3, 1);

    // matrix to make the rectangle come back to the previous point
    Matriz translacaoVolta(3, 3);
    translacaoVolta.inserirValores(1, 1, 1);
    translacaoVolta.inserirValores(1, 2, 0);
    translacaoVolta.inserirValores(1, 3, dx);

    translacaoVolta.inserirValores(2, 1, 0);
    translacaoVolta.inserirValores(2, 2, 1);
    translacaoVolta.inserirValores(2, 3, dy);

    translacaoVolta.inserirValores(3, 1, 0);
    translacaoVolta.inserirValores(3, 2, 0);
    translacaoVolta.inserirValores(3, 3, 1);

    // matrix with the points of the original polygon
    Matriz original(3, numberOfPoints);
    for (int i = 0; i < numberOfPoints; i++)
    {
        original.inserirValores(1, i + 1, points[i].x);
        original.inserirValores(2, i + 1, points[i].y);
        original.inserirValores(3, i + 1, 1);
    }

    Matriz resultado1(3, 3);
    resultado1 = ((translacaoVolta * rotacao) * translacaoOrigem);

    Matriz resultadoFinal(3, 4);
    resultadoFinal = resultado1 * original;

    // sets the new points
    for (int i = 0; i < numberOfPoints; i++)
    {
        points[i].x = resultadoFinal.a[0][i];
        points[i].y = resultadoFinal.a[1][i];
    }

    // fix the colision, recalculates minimo and maximo again
    minimox = maximox = points[0].x;
    minimoy = maximoy = points[0].y;

    for (int i = 0; i < numberOfPoints; i++)
    {
        if (points[i].x < minimox)
            minimox = points[i].x;
        if (points[i].x > maximox)
            maximox = points[i].x;
        if (points[i].y < minimoy)
            minimoy = points[i].y;
        if (points[i].y > maximoy)
            maximoy = points[i].y;
    }
}

double RotatePolygon::getMinX() const
{
    return minimox;
}

double RotatePolygon::getMinY() const
{
    return minimoy;
}

double RotatePolygon::getMaxX() const
{
    return maximox;
}

double RotatePolygon::getMaxY() const
{
    return maximoy;
}
