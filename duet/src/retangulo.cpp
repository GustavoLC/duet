#include "retangulo.h"
#include "matriz.h"
#include <iostream>

using namespace std;

/**
 * 
 * @brief Construct a new Retangulo:: Retangulo object
 * @authors Gustavo Lofrese Carvalho, Eduardo Meneghim
 * @date 1/12/2021
 * @version 11/01/2022
 * @param cimaEsquerda, quina superior esquerda do retangulo 
 * @param base 
 * @param altura 
 */

Retangulo::Retangulo(Point2 cimaEsquerda, double base, double altura, bool deveGirar):
    Quadrilateral()
{
    p[0].x = cimaEsquerda.x;
    p[0].y = cimaEsquerda.y;
    p[1].x = cimaEsquerda.x;
    p[1].y = cimaEsquerda.y + altura;
    p[2].x = cimaEsquerda.x + base;
    p[2].y = cimaEsquerda.y + altura;
    p[3].x = cimaEsquerda.x + base;
    p[3].y = cimaEsquerda.y;
}

Retangulo::~Retangulo()
{}

