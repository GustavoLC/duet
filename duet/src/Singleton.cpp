#include "Singleton.h"

template <typename T>
T& Singleton<T>::getInstance() {
    static T instance;
    return instance;
}

template <typename T>
T& Singleton<T>::getInstance(int n) {
    static T instance(n);
    return instance;
}
