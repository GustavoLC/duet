#include "drawCircle.h"
#include <iostream>

DrawCircle::DrawCircle(Point2 *centro, int raio) : centro(centro), raio{raio}
{
}

void DrawCircle::draw(Window &w)
{
    w.drawCircle((int)centro->x, (int)centro->y, (int)raio);
}