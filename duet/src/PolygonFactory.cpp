
#include "../include/PolygonFactory.h"
#include <iostream>

using namespace std;

Polygon* PolygonFactory::Create(int nPontos, Jogo& pJogo, double pTamanho)
{
    Point2 ponto = Point2(pJogo.sortearNumero(800 * 30 / 100, 800 * 70 / 100), 0);

    double coeficiente = pJogo.sortearNumero(1, 2) == 1 ? 0.5 : 2;

    if (nPontos == 4)
        return new Retangulo(ponto, pTamanho * coeficiente, pTamanho / coeficiente, false);
    //TODO: Novos polígonos

    return new Polygon(nPontos);
}