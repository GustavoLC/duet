#include "polygon.h"
#include "drawPolygon.h"
#include "rotatePolygon.h"
#include <iostream>

using namespace std;

/**
 * Constructor
 * \author Gustavo Lofrese Carvalho, Éder Augusto Penharbel, Henrique Varela
 * \date 13/11/2021
 * \param npontos, number of geometric shape points
 * \param p, array which contains the coordinates of the points
 * \param minimox, minimum value of x
 * \param maximox, maximum value of x
 * \param minimoy, minimum value of y
 * \param maximoy, maximum value of y
 * \internal - 19/11/2021 - aplicação de herança no código do Henrique Varela de colisão de Polygons.
 */
Polygon::Polygon(int npontos, Point2 p[]) : npontos{npontos}
{
    minimoy = minimox = p[0].x;
    maximoy = maximox = p[0].y;

    Polygon::p = new Point2[npontos];

    for (int i = 0; i < npontos; i++)
    {
        if (p[i].x < minimox)
            minimox = p[i].x;
        if (p[i].x > maximox)
            maximox = p[i].x;
        if (p[i].y < minimoy)
            minimoy = p[i].y;
        if (p[i].y > maximoy)
            maximoy = p[i].y;

        Polygon::p[i] = p[i];
    }

    drawBehavior = new DrawPolygon(npontos, p);
    rotateBehavior = new RotatePolygon(npontos, p);
}

/**
 * @brief Construct a new Polygon:: Polygon object
 * @date 13/11/2021
 * @param npontos, number of geometric shape points
 */
Polygon::Polygon(int npontos) : npontos{npontos}
{
    minimoy = minimox = 0;
    maximoy = maximox = 0;

    Polygon::p = new Point2[npontos];

    for (int i = 0; i < npontos; i++)
    {

        Polygon::p[i].x = 0;
        Polygon::p[i].y = 0;
    }

    drawBehavior = new DrawPolygon(npontos, p);
    rotateBehavior = new RotatePolygon(npontos, p);
}

/**
 * \brief Destroy the Polygon:: Polygon object
 */
Polygon::~Polygon()
{
    delete[] p;
    p = nullptr;
}

/**
 * @authors Gustavo Lofrese, Eduardo Meneghim
 * @brief translate the polygon, makes a translate matrix, make a matrix with the points of polygon then multiply both and generate the new points
 * @date 17/12/2021
 * @version 02/02/2022
 * @param dx how much will translate to left or right
 * @param dy how much will translate to up or down
 */
void Polygon::transladar(double dx, double dy)
{
    // matrix to make the polygon translate
    Matriz t(3, 3);
    t.inserirValores(1, 1, 1);
    t.inserirValores(1, 2, 0);
    t.inserirValores(1, 3, dx);
    t.inserirValores(2, 1, 0);
    t.inserirValores(2, 2, 1);
    t.inserirValores(2, 3, dy);
    t.inserirValores(3, 1, 0);
    t.inserirValores(3, 2, 0);
    t.inserirValores(3, 3, 1);

    // matrix with the points of the polygon

    Matriz r(3, npontos);
    for (int i = 0; i < npontos; i++)
    {
        r.inserirValores(1, i + 1, p[i].x);
        r.inserirValores(2, i + 1, p[i].y);
        r.inserirValores(3, i + 1, 1);
    }

    // result matrix
    Matriz b(3, 4);
    b = t * r;

    // sets the new points
    for (int i = 0; i < npontos; i++)
    {
        p[i].x = b.a[0][i];
        p[i].y = b.a[1][i];
    }

    // fix the colision, recalculates minimo and maximo again
    minimox = maximox = p[0].x;
    minimoy = maximoy = p[0].y;

    for (int i = 0; i < npontos; i++)
    {
        if (p[i].x < minimox)
            minimox = p[i].x;
        if (p[i].x > maximox)
            maximox = p[i].x;
        if (p[i].y < minimoy)
            minimoy = p[i].y;
        if (p[i].y > maximoy)
            maximoy = p[i].y;
    }
}

/**
 * @author Gustavo Lofrese Carvalho
 * @brief return the minimum value of x
 * @return double
 * @date 31/01/2022
 * @version 31/01/2022
 */
double Polygon::getMinX() const
{
    return minimox;
}

/**
 * @brief return the minimum value of y
 * @author Gustavo Lofrese Carvalho
 * @return double
 * @date 31/01/2022
 * @version 31/01/2022
 */
double Polygon::getMinY() const
{
    return minimoy;
}

/**
 * @brief return the maximum value of x
 * @author Gustavo Lofrese Carvalho
 * @return double
 * @date 31/01/2022
 * @version 31/01/2022
 */
double Polygon::getMaxX() const
{
    return maximox;
}

/**
 * @brief return the maximum value of y
 * @author Gustavo Lofrese Carvalho
 * @return double
 * @date 31/01/2022
 * @version 31/01/2022
 */
double Polygon::getMaxY() const
{
    return maximoy;
}

/**
 * @brief function to test if there is a collision
 *
 * @param f1, Polygon 1
 * @param f2, Polygon 2
 * @return true - there was a collision
 * @return false - there was no collision
 */
bool Polygon::colisao(Polygon &f1, Polygon &f2)
{
    if (((f2.getMinX() >= f1.getMinX() && f2.getMinX() <= f1.getMaxX()) || (f2.getMaxX() >= f1.getMinX() && f2.getMaxX() <= f1.getMaxX())) &&
        ((f2.getMinY() >= f1.getMinY() && f2.getMinY() <= f1.getMaxX()) || (f2.getMaxX() >= f1.getMinY() && f2.getMaxX() <= f1.getMaxX())))
    {
        cout << "colisao" << endl;
        return true;
    }
    return false;
}

/**
 * @brief Detecta colisão com reta, adaptação do código do Breakout
 * @authors Gustavo Lofrese Carvalho, Eduardo Meneghim
 * @date 09/02/2022
 * @version 09/02/2022
 * @param c
 * @param r
 * @return true
 * @return false
 */
bool Polygon::colisaoR(const Point2 *c, const double r)
{
    for (int i = 0; i < npontos - 1; i++)
    {
        // coordenada x da direçao da reta
        double dx = abs(p[i + 1].x - p[i].x);
        // coordenada y da direçao da reta
        double dy = abs(p[i + 1].y - p[i].y);

        // vetor a inicia do começo da reta até o ponto
        double ax = c->x - p[i].x;
        double ay = c->y - p[i].y;

        // modulo do vetor direção
        double absD = sqrt(dx * dx + dy * dy);
        // cout << "absd: " << absD << endl;

        // vetor unitário da direção
        double unitDx = dx / absD;
        double unitDy = dy / absD;

        // projeção escalar
        double projE = (dx * ax + dy * ay) / absD;

        // projeção vetorial = projeção escalar * unitário da direção
        double projVx = projE * unitDx;
        double projVy = projE * unitDy;

        // complemento ortogonal, co + projV = r1paraP
        double coX = ax - projVx;
        double coY = ay - projVy;

        double absco = sqrt(coX * coX + coY * coY);

        if (absD >= abs(projE) && absco <= r)
            return true;
    }

    return false;
}