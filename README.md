# Duet 

Duet inpired game created as a college project  

![Jogo Duet Rodando](https://i.imgur.com/mTTfCKn.png){width=50%}


## :gear: How to run it:

You have two options to run this project. The first is to open the project within the configured dev container, which already has all the prerequisites pre-installed and our development settings defined. The second is to build locally, which requires manual installation of the prerequisites.

---

### :package: Dev Container 

#### Requirements
- Vscode or other dev container supported IDE 
- Docker 

#### Instructions

If you are using vscode, install the [official dev container extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers) and open the project inside the container: 

Open the command pallete and run ***"Dev Containers: Open Workspace in Container"***

--- 

### :computer: Local Instalation

> **Note:** 
> This project is designed to run primarily on Linux environments with amd64 architecture.

#### Requirements
- C++ 11 compiler
- SDL
- Cmake
- Make

#### Build and run
```sh
cd duet
cmake .
make
./duet
```

---

## :memo: Design Patterns Refactor


In a project for the Design Patterns course in the Computer Science program at the IFC, Blumenau Campus, we performed a refactoring of this old project originally developed in an Object-Oriented Programming course. You can check the [document describing this refactoring by clicking here](
https://docs.google.com/document/d/175Ys0D6m3HUHKlAeBNg7zhOFooT9hLCLlskb9cUtHXY/edit)

## :busts_in_silhouette: Members
- Gustavo L Carvalho - @GustavoLC - project owner
- Eduardo M A Silva - @emeneghim
-  Vítor A U Otto - @vitorueno - joined the project for the refactoring